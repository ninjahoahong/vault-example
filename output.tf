output "vault_vm_public_ip" {
  description = "public IP address of the vault vm"
  value       = "${azurerm_public_ip.vault_public_ip.ip_address}"
}

output "vault_vm_private_ip" {
  description = "private IP address of the vault vm"
  value       = "${azurerm_network_interface.vault_nif.private_ip_address}"
}

output "vault_vm_ssh" {
  description = "shortcut to ssh into the vault vm."
  value = "ssh aduo-trvade-user@${azurerm_public_ip.vault_public_ip.ip_address} -i .ssh/try-vault-on-azure"
}