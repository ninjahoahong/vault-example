variable "subscription_id" {}

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
  default = "North Europe"
}

variable "resource_group_name" {
  default = "aduo-trvade"
}

variable "sg_name" {
  description = "Security group name"
  default = "aduo-trvade-sg"
}

variable "subnet_prefixes" {
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "subnet_names" {
  default = ["aduo-trvade-public-subnet", "aduo-trvade-private-subnet"]
}

variable "install_vault_command" {
  description = "Command to be excuted by the custom script extension"
  default     = "./install-hashicorp-vault.sh 1.0.3"
}

variable "cmd_script" {
  description = "Script to download which can be executed by the custom script extension"
  default     = "https://gist.githubusercontent.com/ninjahoahong/e0f908b9910f18bd0dcd6ff913a34b18/raw/7831b7080c502c3b3f55c1f30d315d64601dbf21/install-hashicorp-vault.sh"
}

variable "ssh_key_public" {
  default = "./.ssh/try-vault-on-azure.pub"
}

variable "vnet_name" {
  description = "Name of the vnet to create"
  default     = "terraform-vault-vnet"
}

variable "address_space" {
  description = "The address space that is used by the virtual network."
  default     = "10.0.0.0/16"
}

variable "dns_servers" {
  description = "The DNS servers to be used with vNet."
  default     = []
}

variable "tags" {
  type        = "map"

  default = {
    environment = "dev"
    terraform = "true"
    owner = "aduo"
  }
}