# Vault example 

* Install vault in Azure VM using Terraform
* Put subscription_id in to terraform.tfvars
* Create key to `.ssh` folder with name `try-vault-on-azure`