provider "azurerm" {
  version           = "=1.22.0"
  subscription_id   = "${var.subscription_id}"
}

resource "azurerm_resource_group" "vault" {
  name      = "${var.resource_group_name}"
  location  = "${var.location}"
  tags      = "${var.tags}"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.vnet_name}"
  location            = "${var.location}"
  address_space       = ["${var.address_space}"]
  resource_group_name = "${azurerm_resource_group.vault.name}"
  dns_servers         = "${var.dns_servers}"
  tags                = "${var.tags}"
}

resource "azurerm_subnet" "subnets" {
  name                      = "${var.subnet_names[count.index]}"
  virtual_network_name      = "${azurerm_virtual_network.vnet.name}"
  resource_group_name       = "${var.resource_group_name}"
  address_prefix            = "${var.subnet_prefixes[count.index]}"
  count                     = "${length(var.subnet_names)}"
}

resource "azurerm_network_security_group" "security_group" {
  name                = "${var.sg_name}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.vault.name}"
  tags                = "${var.tags}"

  security_rule {
    name                        = "ssh"
    priority                    = 102
    direction                   = "Inbound"
    access                      = "Allow"
    protocol                    = "Tcp"
    source_port_range           = "*"
    destination_port_range      = "22"
    source_address_prefix       = "*"
    destination_address_prefix  = "*"
  }

  security_rule {
    name                        = "rdp"
    priority                    = 101
    direction                   = "Inbound"
    access                      = "Allow"
    protocol                    = "Tcp"
    source_port_range           = "*"
    destination_port_range      = "3389"
    source_address_prefix       = "*"
    destination_address_prefix  = "*" 
  }
}

resource "azurerm_public_ip" "vault_public_ip" {
  name                         = "vault_public_ip"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.vault.name}"
  allocation_method            = "Static"
  domain_name_label            = "${var.resource_group_name}-ssh"
  tags                         = "${var.tags}"
}

resource "azurerm_network_interface" "vault_nif" {
  name                      = "vault_nif"
  location                  = "${var.location}"
  resource_group_name       = "${azurerm_resource_group.vault.name}"
  tags                      = "${var.tags}"

  ip_configuration {
    name                          = "IPConfiguration"
    subnet_id                     = "${azurerm_subnet.subnets.0.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.vault_public_ip.id}"
  }
}

resource "azurerm_subnet_network_security_group_association" "vault_network_sg_association" {
  subnet_id                 = "${azurerm_subnet.subnets.*.id[count.index]}"
  network_security_group_id = "${azurerm_network_security_group.security_group.id}"
}

resource "azurerm_virtual_machine" "vault_vm" {
  name                          = "vault_vm"
  location                      = "${var.location}"
  resource_group_name           = "${azurerm_resource_group.vault.name}"
  network_interface_ids         = ["${azurerm_network_interface.vault_nif.id}"]
  vm_size                       = "Standard_DS1_v2"
  delete_os_disk_on_termination = true
  tags                          = "${var.tags}"

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "vault_osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "aduo-trvade"
    admin_username = "aduo-trvade-user"
    admin_password = "Password1234!"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/aduo-trvade-user/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_key_public}")}"
    }
  }

  identity = {
    type = "SystemAssigned"
  }
}

resource "azurerm_virtual_machine_extension" "vault_vm_ex" {
  name                  = "vault_vm_ex"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.vault.name}"
  virtual_machine_name  = "${azurerm_virtual_machine.vault_vm.name}"
  publisher             = "Microsoft.Azure.Extensions"
  type                  = "CustomScript"
  type_handler_version  = "2.0"
  depends_on            = ["azurerm_virtual_machine.vault_vm"]

  settings             = <<SETTINGS
    {
      "commandToExecute": "${var.install_vault_command}",
       "fileUris": [
        "${var.cmd_script}"
       ]
    }
  SETTINGS
}

# Gets the current subscription id
data "azurerm_subscription" "current" {
  subscription_id = "${var.subscription_id}"
}

output "name" {
  value = "${data.azurerm_subscription.current.id}"
}


resource "azurerm_role_assignment" "vault_role_assignment" {
  scope                = "${data.azurerm_subscription.current.id}"
  role_definition_name = "Reader"
  principal_id         = "${lookup(azurerm_virtual_machine.vault_vm.identity[0], "principal_id")}"
}
