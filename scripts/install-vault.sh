#!/bin/bash 

set -e 

VAULT_VERSION=$1

echo "-> Update and Installing dependencies....."
apt-get update
apt-get upgrade -y
apt-get install -y \
  unzip \
  wget


echo "-> Installing Vault ${VAULT_VERSION} ....."
cd /tmp && {
  wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip
  wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_SHA256SUMS
  grep linux_amd64 vault_*_SHA256SUMS | sha256sum -c -
  unzip vault_*.zip
  sudo cp vault /usr/local/bin/
  sudo setcap cap_ipc_lock=+ep /usr/local/bin/vault
  vault --version
  rm -rf vault_*.zip
}